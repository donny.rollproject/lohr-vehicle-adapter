<?php
/*
 * This file is part of the Lohr SSO library.
 *
 *
 */
namespace Narazima\LohrVehicleAdapter\Api;

use Narazima\LohrVehicleAdapter\Adapter\AdapterInterface;

abstract class AbstractApi
{

    const ENDPOINT = 'https://mylohr.fr/sso/public/api';
   
    protected $adapter;
    protected $endpoint;
    protected $meta;
    

    /**
     * @param AdapterInterface $adapter
     * @param string|null      $endpoint
     */
    public function __construct(AdapterInterface $adapter, $endpoint = null)
    {
        $this->adapter = $adapter;
        $this->endpoint = $endpoint ?: static::ENDPOINT;
    }


    /**
     * @param \stdClass $data
     *
     * @return Meta|null
     */
    protected function extractMeta(\StdClass $data)
    {
        if (isset($data->meta)) {
            $this->meta = new Meta($data->meta);
        }
        return $this->meta;
    }

    
    /**
     * @return Meta|null
     */
    public function getMeta()
    {
        return $this->meta;
    }
}