<?php

/**
 * This file is part of the Lohr SSO library.
 * This class represent individual user that registered on Lohr SSO
 * for list of all user consider to look at Contact Entity
 */

namespace Narazima\LohrVehicleAdapter\Api;

use Narazima\LohrVehicleAdapter\Entity\Vehicle as VehicleEntity;

/**
 * @author Narazima <>
 */
class Vehicle extends AbstractApi
{

  public function info()
  {
    $user = $this->adapter->get(sprintf('%s/user', $this->endpoint));
    $user = json_decode($user);
    $user = new UserEntity($user);

    return $user;
  }

  public function findVehicle($clientId)
  {
    $vehicle = $this->adapter->get(sprintf('%s/vehicles/client/'.$clientId, $this->endpoint));
    return json_decode($vehicle);
  }

}
