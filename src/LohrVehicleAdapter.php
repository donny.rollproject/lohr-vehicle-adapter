<?php
/**
 * Main class for Lohr SSO
 */

namespace Narazima\LohrVehicleAdapter;

use Narazima\LohrVehicleAdapter\Adapter\AdapterInterface;
use Narazima\LohrVehicleAdapter\Api\Vehicle;

class LohrVehicleAdapter
{

  protected $adapter;

  protected $endpoint;

  public function __construct(AdapterInterface $adapter, $endpoint = null)
  {
    $this->adapter  = $adapter;
    $this->endpoint = $endpoint;
  }

  public function vehicle()
  {
    return new Vehicle($this->adapter, $this->endpoint);
  }
}
