<?php

/** 
 * Many things to do here
 * See CakeHttpAdapter for sample
 */

namespace Narazima\Adapter;


use Magento\Framework\HTTP\Client\Curl as Client;


class MagentoAdapter implements AdapterInterface {


	/**
     * @var string
     */
    protected $token;


    /**
     * @var string
     */
    protected $response;

    /**
     * @var Cake\Network\Http\Client|null
     */
    protected $client;


    /**
     * @param string               			$token
     * @param Cake\Network\Http\Client|null $client
     */
    public function __construct($token, $client = null)
    {
        $this->token = $token;
        if ( $client == null ) {
            $this->client = new Client();
            $this->client->addHeader('Authorization', 'Bearer ' . $token);
        }
    }

    /******** ************* ******** *****
    * MANY THINGS TODO HERE FOR MAGENTO!!!
    ********* ************* ******** *****/

}