<?php

/**
 * This file is part of the Lohr SSO Api Library
 *
 * @author narazima <info@narazima.com>
 */

namespace Narazima\LohrVehicleAdapter\Adapter;

interface AdapterInterface
{
    
    public function get($url);

    public function delete($url);

    public function post($url, $content = '');

    public function put($url, $content = '');

    public function patch($url, $content = '');

}
