<?php
/** 
 * Many things to do here
 * See CakeHttpAdapter for sample
 */

namespace Narazima\LohrVehicleAdapter\Adapter;

use GuzzleHttp\Client;

class GuzzleHttpAdapter implements AdapterInterface {

	/**
     * @var string
     */
    protected $token;


    /**
     * @var string
     */
    protected $response;

    /**
     * @var Cake\Network\Http\Client|null
     */
    protected $client;


    /**
     * @param string               			$token
     * @param Cake\Network\Http\Client|null $client
     */
    public function __construct($token, $client = null)
    {
        $this->token = $token;
        if ( $client == null ) {
            $this->client = new Client([
                'headers' => ['Authorization' => 'Bearer ' . $token]
            ]);
        }
    }


    /**
     * GET Method
     *
     * @param string     $url
     *
     * @throws HttpException
     */
    public function get($url)
    {
        try {
            $this->response = $this->client->get($url);
        } catch (RequestException $e) {
            $this->response = $e->getResponse();
            $this->handleError();
        }

        return $this->response->getBody();
    }


    /**
     * DELETE Method
     * 
     * @param string        $url
     *
     * @throws HttpException
     */
    public function delete($url)
    {
        try {
            $this->response = $this->client->delete($url);
        } catch (RequestException $e) {
            $this->response = $e->getResponse();
            $this->handleError();
        }

        return $this->response->getBody();
    }


    /**
     * PUT Method
     *
     * @param string        $url
     * @param array         $content 
     *
     * @return string
     ** @throws HttpException
     */
    public function put($url, $content = array() )
    {
        try {
            $this->response = $this->client->put($url, $content);
        } catch (RequestException $e) {
            $this->response = $e->getResponse();
            $this->handleError();
        }

        return $this->response->getBody();
    }


    /**
     * PATCH Method
     *
     * Using pure PATCH method while sending a file is impossible with Laravel Framework
     * using POST method with _method query string can be trick it for a while
     *
     * @param string        $url
     * @param array         $content 
     *
     * @return string
     * @throws HttpException
     */
    public function patch($url, $content = array() )
    {
        try {
            $this->response = $this->client->post($url . '?_method=PATCH', $content);
        } catch (RequestException $e) {
            $this->response = $e->getResponse();
            $this->handleError();
        }

        return $this->response->getBody();
    }

    /**
     * POST Method
     *
     * @param string        $url
     * @param string        $content
     *
     * @return string
     * @throws HttpException
     */
    public function post($url, $content = '')
    {
        try {
            $this->response = $this->client->post($url, $content);
        } catch (RequestException $e) {
            $this->response = $e->getResponse();
            $this->handleError();
        }

        return $this->response->getBody();
    }


    /**
     * @deprecated
     */
    public function getLatestResponseHeaders()
    {
        if (null === $this->response) {
            return;
        }

        return [
            'reset' => (int) (string) $this->response->getHeader('RateLimit-Reset'),
            'remaining' => (int) (string) $this->response->getHeader('RateLimit-Remaining'),
            'limit' => (int) (string) $this->response->getHeader('RateLimit-Limit'),
        ];
    }

    /**
     * @throws HttpException
     */
    protected function handleError()
    {
        $body = (string) $this->response->getBody();
        $code = (int) $this->response->getStatusCode();

        $content = json_decode($body);

        throw new HttpException(isset($content->message) ? $content->message : 'Request not processed.', $code);
    }

}