<?php

namespace Narazima\LohrVehicleAdapter\Entity;

final class Vehicle extends AbstractEntity
{
  /**
   * @var string
   */
  public $client_id;

  /**
   * @var string
   */
  public $vehicle_serial;

  /**
   * @Override
   */
  // public function build(array $parameters)
  // {
  //   foreach ($parameters as $property => $value) {}
  // }

}
